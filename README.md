# Etude_de_cas_mention_IA_CS_3


The objective of the study case is to deal on a very classical Machine Learning task. In particular, you will have to build a review polarity prediction model for an e-commerce firm about their products (here some books). The firm has some historical data on some products that can be used to learn a model. As a consequence, you have chosen to build a data-driven model using machine learning.

In this study case, we have reproduced kind of REAL data contexts! In particular, it means that you have to deal with DIRTY DATA and in this case with distributional shifts between your training data and your production data.

The subject is [here](./Lab_distribution_Shift_student.ipynb)
